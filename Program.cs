﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Snake
{
    enum Directions { Right = 0, Left = 1, Down = 2, Up = 3 };

    struct Position {
        public int x, y;
        public Position(int x, int y) 
        {
            this.x = x;
            this.y = y;
        }
    };

    class Program
    {
        static void Main(string[] args)
        {
            Queue<Position> elements = new Queue<Position>();
            Directions direction = 0;
            Random randnum = new Random();
            double sleepTime = 200.0;

            //Console.BufferHeight = Console.WindowHeight;
            Console.Clear();
            Console.CursorVisible = false;
            Position food = new Position(randnum.Next(0,Console.WindowWidth), randnum.Next(0, Console.WindowHeight));
            Console.SetCursorPosition(food.x, food.y);
            Console.Write("@");

            Position[] pos = { 
                new Position ( 1, 0 ), //right
                new Position (-1, 0 ), //left
                new Position ( 0, 1 ), //down
                new Position ( 0,-1 ), //up
            };

            for (int i = 0; i < 5; i++) {
                elements.Enqueue(new Position (i, 0));
            }

            foreach (Position p in elements)
            {
                Console.SetCursorPosition(p.x, p.y);
                Console.Write("*");
            }

            while (true) 
            {
                if (Console.KeyAvailable)
                {
                    ConsoleKeyInfo key = Console.ReadKey(false);

                    if (key.Key == ConsoleKey.LeftArrow)
                    {
                        if(direction != Directions.Right) direction = Directions.Left;
                    }
                    if (key.Key == ConsoleKey.RightArrow)
                    {
                        if (direction != Directions.Left) direction = Directions.Right;
                    }
                    if (key.Key == ConsoleKey.UpArrow)
                    {
                        if (direction != Directions.Down) direction = Directions.Up;
                    }
                    if (key.Key == ConsoleKey.DownArrow)
                    {
                        if (direction != Directions.Up) direction = Directions.Down;
                    }
                }

                
                Position head = elements.Last();
                Position nextDirection = pos[(int)direction];
                Position headNew = new Position(head.x + nextDirection.x, head.y + nextDirection.y);
                
                if (headNew.x < 0 || headNew.y < 0 
                    || headNew.x == Console.WindowWidth 
                    || headNew.y == Console.WindowHeight
                    || elements.Contains(headNew)
                    )
                {
                    Console.SetCursorPosition(Console.WindowWidth / 2 - 10, Console.WindowHeight / 2);
                    Console.Write("Game Over! - ");
                    Console.WriteLine("Points {0}", elements.Count - 5);
                    Console.ReadLine();
                    return;
                }

                elements.Enqueue(headNew);
                Console.SetCursorPosition(headNew.x, headNew.y);
                Console.Write("*");

                if (food.x == headNew.x && food.y == headNew.y)
                {
                    do
                    {
                        food = new Position(randnum.Next(0, Console.WindowWidth), randnum.Next(0, Console.WindowHeight));
                    } while (elements.Contains(food));

                }
                else
                {
                    Position last = elements.Dequeue();
                    Console.SetCursorPosition(last.x, last.y);
                    Console.Write(" ");
                }

                Console.SetCursorPosition(food.x, food.y);
                Console.Write("@");

                sleepTime -= 0.01;
                Thread.Sleep((int)sleepTime);
            }
        }
    }
}
